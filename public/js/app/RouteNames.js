export default {
  PHOTO_INPUT: "/app/photo-input",
  COLOUR_PICKER_INPUT: "/app/colour-picker",
  RESULT_LIST: "/app/results",
  QUESTION: "/app/question",
  IMAGE_DETAILS: "/app/image/:id",
  PREVIEW: "/app/preview",
  CHECKOUT: "/app/checkout"
};
