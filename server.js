console.log(process.env.NODE_ENV);
if (process.env.NODE_ENV !== "production") {
  console.log("NODE_ENV not prodiction, load env!");
  require("dotenv").load();
}

console.log(process.env.MONGODB_URI);

var express = require("express"),
  path = require("path"),
  bodyParser = require("body-parser"),
  http = require("http"),
  recommendationEngine = require("./app/recommendation-engine"),
  Validator = require("jsonschema").Validator,
  recommendSchema = require("./schemas/criteriaSchema.json"),
  questionnaire = require("./app/interview/questionnaire.json"),
  sslRedirect = require("heroku-ssl-redirect"),
  history = require("connect-history-api-fallback");
var vhost = require("vhost");
var questionSet = [];
var registeredSubdomains = ["pitc.localhost", "pitc.artific.app","wwieteska.localhost","wwieteska.artific.app"];

Object.keys(questionnaire).forEach(function(key, index) {
  var qId = key;
  var qText = questionnaire[key].text;
  var qAnswers = questionnaire[key]["answers"].map(o => o.answer);
  qObj = {
    id: qId,
    question: qText,
    answers: qAnswers
  };
  questionSet.push(qObj);
});

function getSubdomain(host) {
  var subdomain = null;
  if (registeredSubdomains.includes(host)) {
    var subdomain = host.split(".")[0];
  }
  return subdomain;
}

function getQuestions(host) {
  if (registeredSubdomains.includes(host)) {
    return [];
  } else return questionSet;
}

var metadataDao = require("./db/metadata-dao");

var app = express();

app.use(sslRedirect());

var validator = new Validator();

var middleware = history({
  rewrites: [{ from: /^\/app\/.*/, to: "/app.html" }],
  verbose: false
});
app.use(middleware);

for (var index in registeredSubdomains) {
  var host = registeredSubdomains[index];
  var subdomain = getSubdomain(host);
  app.use(
    vhost(
      host,
      express.static(path.join(__dirname, "public", "subdomains", subdomain))
    )
  );
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

app.set("port", process.env.PORT || 3000);

app.get("/questions", function(req, res) {
  res.send(getQuestions(req.hostname));
});

app.post("/recommend", function(req, res) {
  var criteria = req.body;
  criteria.subdomain = getSubdomain(req.hostname);
  var valResult = validator.validate(criteria, recommendSchema);
  if (req.query.c){
    criteria.collectionFilter = req.query.c
  }
  if (valResult.valid) {
    recommendationEngine.getRecommendation(criteria, function(images) {
      res.status(200);
      res.send(images);
    });
  } else {
    res.status(400);
    res.send(valResult.errors);
  }
});

app.get("/selectors", function(req, res) {
  res.send(recommendationEngine.getSelectors());
});

app.get("/image/:id", function(req, res) {
  var id = req.params.id;
  metadataDao.findById(
    id,
    function(item) {
      if (item) {
        res.status(200);

        res.send(item);
      } else {
        res.status(404);
        res.send();
      }
    },
    function(err) {
      res.status(500);
      res.send(err);
    }
  );
});

app.get("/arconfig",function(req,res){
  // TODO: refactor this quick and dirty hardcoding
  var subdomain = getSubdomain(req.hostname)
  var arConfig = {}
  arConfig['frameConfig'] = {
    renderMat: true,
    showFrame:true,
    depth: 0.1,
    width: 0.02,
    matPad: 0.2,
    frameColour: "#fcf0d1",
    matColour: "#fcf0d1"
  }
  if (subdomain == "wwieteska"){
    arConfig.frameConfig.renderMat = false;
    arConfig.frameConfig.showFrame = false;
  }
  res.status(200);
  res.send(arConfig)
});

server = http.createServer(app);

server.listen(app.get("port"), function() {
  console.log("Express server listening on port " + app.get("port"));
});
