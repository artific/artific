FILE_FOLDER = "/home/pit/workspace/artific_art_data/pitc/resized/";

METADATA_FILE = "/home/pit/workspace/artific_art_data/pitc/metadata.txt";

DROP_COLLECTION = false;
UPDATE_DB = true;

var metadataParser = require("./metadata-parser");
var colourParser = require("./../common/colour-parser");
var metadataDao = require("./../../db/metadata-dao");

function getImgURL(filename) {
  return FILE_FOLDER + filename;
}

var total = 0;
var finished = 0;

metadataDao.initDB(DROP_COLLECTION, function() {
  metadataParser.parse(
    METADATA_FILE,
    null,
    function(record) {
      let imgFile = FILE_FOLDER + record.filename;

      total++;

      colourParser.parse(
        imgFile,
        record,
        function(colourMetadata) {
          // if file not found, the dominant colour will be set to a default value #000000 and palette will not be available
          // do not put such data into DB
          if (colourMetadata["colours"]) {
            // console.log(record);
            if (UPDATE_DB) {
              metadataDao.addObject(
                colourMetadata,
                function(createdObject) {
                  finished++;
                  console.log(
                    `INSERTED ${
                      createdObject.filename
                    }: ${finished} out of ${total}`
                  );
                },
                function(err) {
                  console.log(
                    `Error on ${
                      colourMetadata.filename
                    }: ${finished} out of ${total}`
                  );
                }
              );
            } else {
              // console.log(record);
              finished++;
              console.log(
                `Finished ${
                  colourMetadata.filename
                }: ${finished} out of ${total}`
              );
            }
          } else {
            finished++;
            console.log(`Not found ${imgFile}: ${finished} out of ${total}:`);
          }
        },
        function() {
          console.log(`error when parsing colour for ${record.filename}`);
          finished++;
        }
      );
    },
    function() {
      console.log("Finished parsing metadata file");
    }
  );
});
