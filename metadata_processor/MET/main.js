const DROP_COLLECTION = false;
const UPDATE_DB = true;
const GET_METADATA = "get met";
const DOWNLOAD_IMGS = "download";
const PARSE_COLOURS = "parse colours";
const FILE_FOLDER = "/home/pit/workspace/artific_art_data/MET";

var metadataParser = require("./metadata-parser");
var colourParser = require("./../common/colour-parser");
var imageDownloader = require("./../common/img-downloader");
var metadataDao = require("./../../db/metadata-dao");
var fs = require("fs");

function getImgURL(filename) {
  return FILE_FOLDER + filename;
}

var finishedXML = false;
var total = 0;
var finished = 0;
var material = "Photographs"; // other options: Paintings, Photographs
var mode = PARSE_COLOURS;

/*
 Metadata creation consists of 3 separate steps manually triggered:
 1. Load image metadata over MET API to database (mode=GET_METADATA)
 2. Download images to a local folder (mode=DOWNLOAD_IMAGES)
 3. Enhance stored metadata with parsed colours (mode=PARSE_COLOURS)
*/

metadataDao.initDB(DROP_COLLECTION, function() {
  if (mode == GET_METADATA) {
    metadataParser.parseSearched(
      material,
      true,
      function(record) {
        console.log(`Parsed ${record.title}`);
        if (record && UPDATE_DB) {
          metadataDao.addObject(
            record,
            function(createdObject) {},
            function(err) {
              console.log(
                `Error on ${record.filename}: ${finished} out of ${total}`
              );
            }
          );
        }
      },
      function() {
        console.log("Finished!");
      }
    );
  }
  if (mode == DOWNLOAD_IMGS) {
    metadataDao.findImages(
      null,
      1000,
      function(items) {
        var imageURLs = items.map(item => item.fileURL);
        imageDownloader.downloadImages(FILE_FOLDER, imageURLs);
      },
      function(err) {
        console.log(err);
      }
    );
  }
  if (mode == PARSE_COLOURS) {
    metadataDao.findImages(
      null,
      1000, // no limit!
      function(items) {
        for (var index in items) {
          var record = items[index];
          var filename = imageDownloader.extractFilename(record.fileURL);
          var localFile = FILE_FOLDER + "/" + filename;
          colourParser.parse(
            localFile,
            record,
            function(targetMetadata) {
              // if file not found, the dominant colour will be set to a default value #000000 and palette will not be available
              // do not put such data into DB

              var record = targetMetadata;

              if (UPDATE_DB) {
                metadataDao.updateImage(
                  record,
                  function(updatedObj) {
                    console.log(`updated ${updatedObj.title}`);
                  },
                  function(err) {
                    console.log(err);
                  }
                );
              }
              // console.log(record);
              // if (UPDATE_DB) {
              //   metadataDao.addObject(
              //     record,
              //     function(createdObject) {
              //       finished++;
              //       console.log(
              //         `INSERTED ${
              //           createdObject.filename
              //         }: ${finished} out of ${total}`
              //       );
              //     },
              //     function(err) {
              //       console.log(
              //         `Error on ${record.filename}: ${finished} out of ${total}`
              //       );
              //     }
              //   );
              // } else {
              //   // console.log(record);
              //   finished++;
              //   console.log(
              //     `Finished ${record.filename}: ${finished} out of ${total}`
              //   );
              // }
            },
            function() {
              console.log(`error when parsing colour for ${record.filename}`);
              finished++;
            }
          );
        }
      },
      function(err) {
        console.log(err);
      }
    );
  }
});

// metadataDao.initDB(DROP_COLLECTION, function() {
//   metadataParser.parse(
//     API_URL,
//     null,
//     function(record) {
//       if (record) {
//         if (UPDATE_DB) {
//           metadataDao.addObject(
//             record,
//             function(createdObject) {
//               finished++;
//               // console.log(`inserted ${finished}`);
//               // console.log(
//               //   `INSERTED ${
//               //     createdObject.filename
//               //   }: ${finished} out of ${total}`
//               // );
//             },
//             function(err) {
//               console.log(
//                 `Error on ${record.filename}: ${finished} out of ${total}`
//               );
//             }
//           );
//         } else {
//           // console.log(record);
//           finished++;
//           console.log(
//             `Finished ${record.filename}: ${finished} out of ${total}`
//           );
//         }
//       }
//       // colourParser.parse(
//       //   imgFile,
//       //   function(colourMetadata) {
//       //     // if file not found, the dominant colour will be set to a default value #000000 and palette will not be available
//       //     // do not put such data into DB
//       //     if (colourMetadata["palette"]) {
//       //       record["colours"] = colourMetadata;
//       //       // console.log(record);
//       //       if (UPDATE_DB) {
//       //         metadataDao.addObject(
//       //           record,
//       //           function(createdObject) {
//       //             finished++;
//       //             console.log(
//       //               `INSERTED ${
//       //                 createdObject.filename
//       //               }: ${finished} out of ${total}`
//       //             );
//       //           },
//       //           function(err) {
//       //             console.log(
//       //               `Error on ${record.filename}: ${finished} out of ${total}`
//       //             );
//       //           }
//       //         );
//       //       } else {
//       //         // console.log(record);
//       //         finished++;
//       //         console.log(
//       //           `Finished ${record.filename}: ${finished} out of ${total}`
//       //         );
//       //       }
//       //     } else {
//       //       finished++;
//       //       console.log(`Not found ${imgFile}: ${finished} out of ${total}:`);
//       //     }
//       // },
//       // function() {
//       //   console.log(`error when parsing colour for ${record.filename}`);
//       //   finished++;
//       // }
//       // );
//     },
//     function(dictionary) {
//       console.log("Finished parsing metadata file " + finished);
//       console.log(dictionary);
//     }
//   );
// });
