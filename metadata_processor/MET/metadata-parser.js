IMG_FORMAT = ".jpg";
NOT_FOUND = "unknown";

var Metadata = require("./../common/Metadata");
const axios = require("axios");
var objectIDs;
var lastbjectID = 169805;
var currentIndex;
var processed = 0;
var parsed = 0;
var total = null;
const PARALLEL_REQUESTS = 50;
var objectNames = new Set();
var currentSearchBatch = 0;
const SEARCH_BATCH_SIZE = 80;
var searchTotalResults = 0;
var searchProcessed = 0;
API_URL = "https://collectionapi.metmuseum.org/public/collection/v1/objects";

function extractSingleMetadata(record) {
  if (record.isPublicDomain) {
    var metadata = new Metadata();
    metadata.filename = record.objectID;
    metadata.title = record.title;
    metadata.isHighlight = record.isHighlight;
    metadata.author = record.artistDisplayName;
    metadata.year = record.objectEndDate;
    metadata.setPeriod();
    metadata.physical_descr = record.medium;
    // TODO: extract x and y dimension from:
    // 28 7/8 × 36 3/4 in. (73.2 × 93.4 cm)
    metadata.dimension_x = 0;
    metadata.dimension_y = 0;
    // metadata.setRatio();
    metadata.institution = "The Metropolitan Museum of Art";

    metadata.institutionURL = "https://www.metmuseum.org/";
    metadata.permalink = record.objectURL;
    metadata.bigFileURL = record.primaryImage;
    metadata.fileURL = record.primaryImageSmall;
    metadata.classification = record.classification;
    metadata.culture = record.culture;
    return metadata;
  } else {
    return null;
  }
}

function getSingleObject(objectID, callback, err) {
  var objectURL = API_URL + "/" + objectID;
  axios
    .get(objectURL)
    .then(response => {
      var metadata = extractSingleMetadata(response.data);
      callback(metadata);
    })
    .catch(error => {
      err(error);
    });
}

function getNextRecords(callbackOnRecord, callbackOnFinish) {
  var i;
  var currentBatch = PARALLEL_REQUESTS;
  console.log(`get next batch: ${PARALLEL_REQUESTS}`);
  for (i = 0; i < PARALLEL_REQUESTS; i++) {
    var objectID = objectIDs[currentIndex];
    currentIndex++;
    getSingleObject(
      objectID,
      function(metadata) {
        currentBatch--;
        processed++;
        callbackOnRecord(metadata);
        if (processed >= total) {
          callbackOnFinish(objectNames);
        } else if (currentBatch == 0) {
          getNextRecords(callbackOnRecord, callbackOnFinish);
        }
      },
      function(error) {
        console.log(error);
        currentBatch--;
        processed++;
        if (processed >= total) {
          callbackOnFinish(objectNames);
        } else if (currentBatch == 0) {
          getNextRecords(url, callbackOnRecord, callbackOnFinish);
        }
      }
    );
  }
}

exports.parse = function(err, callbackOnRecord, callbackOnFinish) {
  axios.get(API_URL).then(response => {
    var data = response.data;
    objectIDs = data.objectIDs;
    if (!total) {
      total = data.total;
    }
    if (lastbjectID) {
      var lastIndex = objectIDs.indexOf(lastbjectID);
      currentIndex = lastIndex + 1;
      processed = lastIndex;
      parsed = lastIndex;
    } else {
      processed = 0;
      currentIndex = 0;
    }
    getNextRecords(url, callbackOnRecord, callbackOnFinish);
  });
};

function searchNext(
  material,
  onlyHighlights,
  callbackOnRecord,
  callbackOnFinish
) {
  var showOnly = "openAccess";
  if (onlyHighlights) {
    showOnly += "%7highlights";
  }
  var offset = currentSearchBatch * SEARCH_BATCH_SIZE;
  var batchResult = 0;
  var batchProcessed = 0;
  var url = `https://metmuseum.org/api/collection/collectionlisting?showOnly=${showOnly}&perPage=${SEARCH_BATCH_SIZE}&offset=${offset}&pageSize=0&material=${material}`;
  var idExtractRegexp = /art\/collection\/search\/(.*)\?/;

  axios
    .get(url, { withCredentials: true })
    .then(response => {
      var data = response.data;
      searchTotalResults = data.totalResults;
      batchResult = data.results.length;
      console.log(
        `Batch no.${currentSearchBatch}, offset:${offset}, batch size:${batchResult}/${searchTotalResults}`
      );
      // full batch processed, enough!
      if (batchResult.length == 0) {
        callbackOnFinish();
      } else {
        for (var index in data.results) {
          var result = data.results[index];
          var extractedID = result.url.match(idExtractRegexp)[1];
          getSingleObject(extractedID, callbackOnRecord, function(err) {
            console.log(err);
          });
          batchProcessed++;
          if (batchProcessed == batchResult) {
            currentSearchBatch++;
            searchNext(
              material,
              onlyHighlights,
              callbackOnRecord,
              callbackOnFinish
            );
          }
        }
      }
    })
    .catch(error => {
      console.log(error);
    });
}

exports.parseSearched = function(
  material,
  onlyHighlights,
  callbackOnRecord,
  callbackOnFinish
) {
  currentSearchBatch = 0;
  searchNext(material, onlyHighlights, callbackOnRecord, callbackOnFinish);
};
