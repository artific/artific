var imageDownloader = require("./../common/img-downloader");
var metadataDao = require("./../../db/metadata-dao");
var Metadata = require("./Metadata");

const DROP_COLLECTION = false;
const MET = "The Metropolitan Museum of Art";
const STAEDEL = "Städel Museum";
const MKG = "The Museum für Kunst und Gewerbe Hamburg";

metadataDao.initDB(DROP_COLLECTION, function() {
  metadataDao.findImages(null, 10000, function(items) {
    for (var index in items) {
      var record = items[index];
      var harmonizedMetadata = new Metadata();
      harmonizedMetadata.copyValues(record);
      var update = false;
      if (harmonizedMetadata.institution == MET) {
        // var filename = imageDownloader.extractFilename(
        //   harmonizedMetadata.fileURL
        // );
        // harmonizedMetadata.fileURL = "/previews/met/" + filename;
      }
      if (harmonizedMetadata.institution == STAEDEL) {
        var filename = harmonizedMetadata.filename.replace(".jpg", "");
        harmonizedMetadata.permalink =
          "http://staedelmuseum.de/go/ds/" + filename;
        harmonizedMetadata.bigFileURL = `https://storage.luckycloud.de/d/9b4540c37b684629ab09/files/?p=/big/${
          harmonizedMetadata.filename
        }&dl=1`;
        harmonizedMetadata.fileURL =
          "/previews/staedel/" + harmonizedMetadata.filename;
        update = true;
      }
      if (harmonizedMetadata.institution == MKG) {
        var filename = imageDownloader.extractFilename(
          harmonizedMetadata.fileURL
        );
        if (filename) {
          harmonizedMetadata.fileURL = "/previews/mkg/" + filename;
        } else {
          harmonizedMetadata.fileURL = null;
          harmonizedMetadata.bigFileURL = null;
        }
        update = true;
      }

      if (update) {
        metadataDao.updateImage(
          harmonizedMetadata,
          function(obj) {
            console.log(`${obj.title} updated`);
          },
          function(err) {
            console.log(err);
          }
        );
      }
    }
  });
});
