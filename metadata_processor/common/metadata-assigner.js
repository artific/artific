const FOLDER = "/home/pit/workspace/artific_art_data/mnk";
const METADATA_FILE = "metadata.json";
// TODO: dynamic folders
const subfolders = [
  "peasants",
  "animals",
  "wealth",
  "cityscapes",
  "devotional",
  "everyday",
  "landscapes",
  "love_erotic",
  "music",
  "seascapes",
  "still_life",
  "war"
];

const APPEND = true;

const RESET = true;
const dataSet = "mnk";

var metadataDao = require("./../../db/metadata-dao");
var fs = require("fs");
var path = require("path");
var DROP_COLLECTION = false;

function getAttributeAndValue(filename) {
  return ["fileURL", `https://res.cloudinary.com/dwwnisj43/image/upload/v1550078599/preview/${dataSet}/${filename}`];
}

function resetMetadata(record) {
  record.motif = null;
  record.associations = null;
  record.content = null;
  record.atmosphere = null;
  record.emotions = null;
  return record;
}

function enhanceMetadata(record, newMetadata) {
  for (var key in newMetadata) {
    if (APPEND && record[key]) {
      var value = record[key];
      if (Array.isArray(value)) {
        record[key] = record[key].concat(newMetadata[key]);
      } else {
        var newArray = [];
        newArray.push(record[key]);
        newArray.push(...newMetadata[key]);
        record[key] = newArray;
      }
    } else {
      record[key] = newMetadata[key];
    }
  }
  return record;
}

var todoList = {};

metadataDao.initDB(DROP_COLLECTION, function() {
  for (var index in subfolders) {
    var targetDir = FOLDER + "/" + subfolders[index];
    console.log(`------- checking ${subfolders[index]} ----------`);
    var metadataFull = targetDir + "/" + METADATA_FILE;
    var newMetadata = require(metadataFull);
    var files = fs.readdirSync(targetDir);
    files.forEach(function(file, indx) {
      if (!file.includes(".json")) {
        var tuple = getAttributeAndValue(file);
        if (todoList[file]) {
          todoList[file].metadata.push(newMetadata);
        } else {
          todoList[file] = {};
          todoList[file].metadata = [newMetadata];
          todoList[file].searchTuple = tuple;
        }
      }
    });
  }
  for (var key in todoList) {
    let tuple = todoList[key].searchTuple;
    let newMetadataList = todoList[key].metadata;
    metadataDao.findByAttribute(tuple[0], tuple[1], function(foundRecord) {
      if (foundRecord) {
        var enhancedRecord = {};
        if (RESET) {
          foundRecord = resetMetadata(foundRecord);
        }
        for (var index in newMetadataList) {
          enhancedRecord = enhanceMetadata(foundRecord, newMetadataList[index]);
        }

        metadataDao.updateImage(enhancedRecord, function(updatedObj) {
          console.log(`updated ${updatedObj.title}`);
        });
      }
    });
  }
});
