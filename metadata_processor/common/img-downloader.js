const axios = require("axios");
const fs = require("fs");
// TOO implement some throttling
const THROTTLE = 1000;
const FILENAME_REGEXP = /\/([^\/]*\.(jpg|png))$/;

exports.extractFilename = function(url) {
  var extractedFilename = null;
  if (url) {
    var match = url.match(FILENAME_REGEXP);
    if (match) {
      extractedFilename = match[1];
    }
  }
  return extractedFilename;
};

exports.downloadImage = function(directory,imageUrl,filename,onFinishCb){
  axios.get(imageUrl, { responseType: "stream" }).then(response => {
    var targetPath = directory + "/" + filename;
    // console.log(`storing ${filename} from ${response.request.path}`);
    var ws = fs.createWriteStream(targetPath);
    response.data.pipe(ws);
    ws.on('finish', ()=>{
      console.log(`finished ${filename}`);
      onFinishCb();
    });
  });
}

exports.downloadImages = function(directory, imageURLs) {
  for (var index in imageURLs) {
    var url = imageURLs[index];

    axios.get(url, { responseType: "stream" }).then(response => {
      var filename = response.request.path.match(FILENAME_REGEXP)[1];
      var targetPath = directory + "/" + filename;
      console.log(`storing ${filename} from ${response.request.path}`);
      response.data.pipe(fs.createWriteStream(targetPath));
    });
  }
};
