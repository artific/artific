module.exports = class Metadata {
  constructor() {
    this.filename = "";
    this.title = "";
    this.author = "";
    this.year = "";
    this.physical_descr = "";
    this.dimension_x = 0;
    this.dimension_y = 0;
    this.ratio = 0;
    this.emotions = [];
    this.period = "";
    this.genre = "";
    this.motif = "";
    this.content = [];
    this.associations = [];
    this.atmosphere = [];
    this.institution = "";
    this.institutionURL = "";
    this.printURL = "";
    this.fileURL = "";
    this.bigFileURL = "";
    this.permalink = "";
    this.isHighlight = false;
    this.culture = "";

    this.colours = { prominent: {} };
    this.subdomain = null;
    this.license = "";
  }

  copyValues(srcMetadata) {
    this.filename = srcMetadata.filename || "";
    this.title = srcMetadata.title || "";
    this.author = srcMetadata.author || "";
    this.year = srcMetadata.year || "";
    this.physical_descr = srcMetadata.physical_descr || "";
    this.dimension_x = srcMetadata.dimension_x || 0;
    this.dimension_y = srcMetadata.dimension_y || 0;
    this.ratio = srcMetadata.ratio || 0;
    this.emotions = srcMetadata.emotions || [];
    this.period = srcMetadata.period || "";
    this.genre = srcMetadata.genre || "";
    this.motif = srcMetadata.motif || "";
    this.content = srcMetadata.content || [];
    this.associations = srcMetadata.associations || [];
    this.atmosphere = srcMetadata.atmosphere || [];
    this.institution = srcMetadata.institution || "";
    this.institutionURL = srcMetadata.institutionURL || "";
    this.printURL = srcMetadata.printURL || "";
    this.fileURL = srcMetadata.fileURL || "";
    this.bigFileURL = srcMetadata.bigFileURL || "";
    this.permalink = srcMetadata.permalink || "";
    this.isHighlight = srcMetadata.isHighlight || false;
    this.culture = srcMetadata.culture || "";
    this.license = srcMetadata.license || "";
    if (srcMetadata) {
      this._id = srcMetadata._id;
    }
    this.colours = srcMetadata.colours || { prominent: {} };
  }

  setRatio() {
    this.ratio =
      Number.parseFloat(this.dimension_x) / Number.parseFloat(this.dimension_y);
  }
  setPeriod(year) {
    var year = year || this.year;
    var yearEx = /\d{4}/
    var yearInt = yearEx.exec(year)
    var century = Math.floor(parseInt(yearInt) / 100) + 1;
    this.period = `${century}th century`;
  }
};
