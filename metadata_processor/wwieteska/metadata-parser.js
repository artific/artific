IMG_FORMAT = ".jpg";
NOT_FOUND = "unknown";
ENGLISH_INDEX = 1;
GERMAN_INDEX = 0;
X_DIM_INDEX = 1;
Y_DIM_INDEX = 0;
var Metadata = require("./../common/Metadata");
var fs = require("fs");
var CLOUDINARY_URL =
  "https://res.cloudinary.com/dwwnisj43/image/upload/v1550078599/preview/";

function parseLine(line) {
  var attributes = line.split(";");
  var record = new Metadata();
  record.author = "Wojtek Wieteska";
  record.fileURL = CLOUDINARY_URL + "wwieteska/" + attributes[0];
  record.filename = attributes[0];
  record.institution = "Wojtek Wieteska Photography";
  record.institutionURL = "http://wojtekwieteska.pl";
  record.title = attributes[2];
  record.year = attributes[1];
  record.permalink = attributes[3];
  record.subdomain = "wwieteska";
  record.license = "Copyright"
  return record;
}

exports.parse = function(file, err, callbackOnRecord, callbackOnFinish) {
  fs.readFile(file, "utf8", function(err, data) {
    var lines = data.split("\n");
    for (index in lines) {
      var record = parseLine(lines[index]);
      callbackOnRecord(record);
    }
    callbackOnFinish();
  });
};
