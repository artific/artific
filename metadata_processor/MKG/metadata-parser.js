const axios = require("axios");
var html2json = require("html2json").html2json;
var fs = require("fs");

var xml2js = require("xml2js");
var Metadata = require("./../common/Metadata");

const CORE_URL = "http://sammlungonline.mkg-hamburg.de";
const LIST_FILE =
  "/home/pit/workspace/digital_art_consultant/metadata_processor/MKG/search_response.html";

var xpath = require("xpath");
var dom = require("xmldom").DOMParser;
const UNKOWN = "unknown";

const FIXED_TITLE_XP =
  "/html[1]/body[1]/div[3]/div[1]/div[1]/div[1]/div[1]/header[1]/h1[1]";

const FIXED_IMG_SRC = "//figure//img";

const MGK_URL = "https://www.mkg-hamburg.de";
const MGK_FULLNAME = "The Museum für Kunst und Gewerbe Hamburg";

function getValue(doc, path) {
  var nodes = xpath.select(path, doc);
  if (nodes && nodes.length > 0) {
    var textContent = nodes[0].textContent;
    var regex = /\n\s*/;
    var afterRegex = textContent.replace(regex, "");

    return afterRegex;
  } else {
    return UNKOWN;
  }
}

function getAttribute(doc, path, searchedAttribute) {
  var nodes = xpath.select(path, doc);
  if (nodes && nodes.length > 0) {
    var attributes = nodes[0].attributes;
    for (var index in attributes) {
      var attribute = attributes[index];
      if (attribute.name == searchedAttribute) {
        return attribute.nodeValue;
      }
    }
    var regex = /\n\s*/;
    var afterRegex = textContent.replace(regex, "");

    return afterRegex;
  } else {
    return UNKOWN;
  }
}

function getValueByLabel(doc, label, complex) {
  var xpath = "";
  if (complex) {
    xpath = `//*[contains(text(),'${label}')]/following-sibling::*/a`;
  } else {
    xpath = `//*[contains(text(),'${label}')]/following-sibling::*`;
  }

  var value = getValue(doc, xpath);
  return value;
}
/*
special function to extract year from following:
"    Emmanuel Orazi     (1860–1934, Künstler/in)     GND1900, Frankreich            "

*/
function extractYear(text) {
  var regexp = /GND(\d{4})/;
  var match = text.match(regexp);
  if (match) {
    return match[1] || UNKOWN;
  } else {
    return UNKOWN;
  }
}

function getAttributeArrayByLabel(doc, lavel, complex) {}

function extractDetails(url, callbackOnRecord) {
  axios.get(url).then(response => {
    var doc = new dom().parseFromString(response.data);

    var metadata = new Metadata();

    var title = getValue(doc, FIXED_TITLE_XP);
    // title

    metadata.title = title;

    // // id
    metadata.filename = getValueByLabel(doc, "Inventory number:", false);

    // // author
    metadata.author = getValueByLabel(doc, "Designing", true);

    var yearString = getValueByLabel(doc, "Designing", false);
    metadata.year = extractYear(yearString);

    // // image

    var imgSrc = getAttribute(doc, FIXED_IMG_SRC, "src");
    metadata.fileURL = imgSrc;
    metadata.bigFileURL = imgSrc;
    metadata.period = getValueByLabel(doc, "Period/Style:", false);
    metadata.institutionURL = MGK_URL;
    metadata.institution = MGK_FULLNAME;
    var permalink = getValueByLabel(doc, "Permalink", true);
    metadata.permalink = permalink;
    callbackOnRecord(metadata);
  });
}

function extractSubject(item, callbackOnRecord) {
  var url = item.permalink;
  if (url) {
    let metadataToUpdate = item;
    axios.get(url).then(response => {
      var doc = new dom().parseFromString(response.data);
      var subject = getValueByLabel(doc, "Subject:", false).split(",");
      var regex = /\n\s*/;
      var leadingWhitespace = /^\s/;
      var trailingWhitespace = /\s*$/;
      var manyWhitespaces = /\s{2,}/;
      var brakets = /\(.*\)/;
      var braketLeft = /\(.*/;
      var braketRigh = /.*\)/;
      var minus = /-.*/;
      var specialChars = /(\n|\t)+/;
      var cleanedUpContent = [];
      for (var index in subject) {
        subject[index] = subject[index].replace(regex, "");
        subject[index] = subject[index].replace(leadingWhitespace, "");
        subject[index] = subject[index].replace(trailingWhitespace, "");
        subject[index] = subject[index].replace(brakets, "");
        subject[index] = subject[index].replace(braketLeft, "");
        subject[index] = subject[index].replace(braketRigh, "");
        subject[index] = subject[index].replace(minus, "");
        subject[index] = subject[index].replace(specialChars, "");
        subject[index] = subject[index].replace(manyWhitespaces, "");
        if (subject[index].length > 0) {
          cleanedUpContent.push(subject[index]);
        }
      }
      metadataToUpdate.content = cleanedUpContent;
      callbackOnRecord(metadataToUpdate);
    });
  }
}

exports.parse = function(callbackOnRecord, callbackOnFinish) {
  var file = LIST_FILE;
  var linkRegexp = /^([^?]*)?/;
  fs.readFile(file, "utf8", function(err, data) {
    var json = html2json(data);
    var results = json.child[0].child;
    for (var index in results) {
      var record = results[index];

      if (record.node == "element") {
        var rawLink = record.child[1].child[1].attr.href;
        var match = rawLink.match(linkRegexp);
        var extractedLink = match[0];
        var fullUrl = CORE_URL + extractedLink;
        extractDetails(fullUrl, callbackOnRecord);
      }
    }
  });
};

exports.parseMore = function(item, callbackOnRecord) {
  extractSubject(item, callbackOnRecord);
};
