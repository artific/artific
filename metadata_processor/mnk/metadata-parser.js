IMG_FORMAT = ".jpg";
NOT_FOUND = "unknown";

var Metadata = require("./../common/Metadata");
const axios = require("axios");
var objectIDs;
var lastbjectID = 169805;
var currentIndex;
var processed = 0;
var parsed = 0;
var total = null;
const PARALLEL_REQUESTS = 50;
var objectNames = new Set();
var currentSearchBatch = 0;
const SEARCH_BATCH_SIZE = 80;
var searchTotalResults = 0;
var searchProcessed = 0;

var COLLECTION_URL="https://zbiory.mnk.pl/en/highlights/catalog/"
var BIG_FILE_URL="https://zbiory.mnk.pl/upload/multimedia/"
var PREVIEW_URL = "https://res.cloudinary.com/dwwnisj43/image/upload/v1550078599/preview/mnk/"

function extractMetadata(record) {
  
  var metadata = new Metadata();
  metadata.filename = record.id;
  metadata.title = record.title;
  metadata.isHighlight = record.masterpieces;
  metadata.author = record.authors[0].name.replace('nieznany','Author unknown');
  metadata.year = record.createDates[0].name.replace('około','ca. ');
  metadata.setPeriod();
  metadata.dimension_x = 0;
  metadata.dimension_y = 0;
  metadata.institution = "Muzeum Narodowe w Krakowie";

  metadata.institutionURL = "https://zbiory.mnk.pl";
  metadata.permalink = COLLECTION_URL+metadata.filename;
  metadata.bigFileURL = BIG_FILE_URL+record.image.filePath+"."+record.image.extension;
  metadata.fileURL = PREVIEW_URL+record.id+".jpg";
  metadata.classification = null;
  metadata.culture = null;
  metadata.license = "Public domain";
  delete metadata.subdomain;
  return metadata;
  
}


function searchAll(
  callbackOnRecord,
  callbackOnFinish
) {
  
  var url = `https://zbiory.mnk.pl/api/search/Object/page/1?maxPerPage=300&filter[masterpieces]=1&filter[types][]=113913`;

  axios
    .get(url)
    .then(response => {
      var data = response.data;
      
      if (data["data"]["items"]) {
        for (var currentIndex in data["data"]["items"]) {
          var record = data["data"]["items"][currentIndex]
          recordMetadata = extractMetadata(record)
          callbackOnRecord(recordMetadata)
        }
      }
      callbackOnFinish()
    })
    .catch(error => {
      console.log(error);
    });
}

exports.parseSearched = function(
  callbackOnRecord,
  callbackOnFinish
) {
  searchAll(callbackOnRecord, callbackOnFinish);
};
