const DROP_COLLECTION = false;
const UPDATE_DB = true;
const GET_METADATA = "get met";
const DOWNLOAD_IMGS = "download";
const PARSE_COLOURS = "parse colours";
const FILE_FOLDER = "/home/pit/workspace/artific_art_data/mnk/preview";

// require("dotenv").load();
console.log(process.env.MONGODB_URI);

var metadataParser = require("./metadata-parser");
var colourParser = require("./../common/colour-parser");
var imageDownloader = require("./../common/img-downloader");
var metadataDao = require("./../../db/metadata-dao");
var fs = require("fs");



function getImgURL(filename) {
  return FILE_FOLDER + filename;
}
 // other options: Paintings, Photographs
var mode = PARSE_COLOURS;

/*
 Metadata creation consists of 3 separate steps manually triggered:
 1. Download images to a local folder (mode=DOWNLOAD_IMAGES)
 2. Enhance stored metadata with parsed colours and store metadata (mode=PARSE_COLOURS)
*/

var queue = []
var queueIndex = -1
function downloadNext(){
  
  queueIndex++
  if (queueIndex == queue.length){
    console.log("Finished!");
    return;
  }
  else{
    var record =queue[queueIndex];
    if (record){
      var filename = record.filename+".jpg";
      console.log("download "+filename);
      imageDownloader.downloadImage(FILE_FOLDER,record.bigFileURL,filename, downloadNext)
    }
  }
}
if (mode == DOWNLOAD_IMGS){
  metadataParser.parseSearched(function(record){
    queue.push(record)
  },
  function(){
    console.log('Parsing API finished! '+queue.length);
    downloadNext(downloadNext);
  }
  );
}

if (mode == PARSE_COLOURS){
  metadataDao.initDB(DROP_COLLECTION, function() {
    metadataParser.parseSearched(function(record){
      var localFile = FILE_FOLDER+"/"+record.filename+".jpg";
      colourParser.parse(
                    localFile,
                    record,
                    function(targetMetadata) {
                      // if file not found, the dominant colour will be set to a default value #000000 and palette will not be available
                      // do not put such data into DB  
                      var record = targetMetadata;
                      if (UPDATE_DB){
                      metadataDao.addObject(
                                    record,
                                    function(createdObject) {
                                      console.log(`Created ${createdObject.filename}`)
                                    },
                                    function(err) {
                                      console.log(
                                        `Error on ${record.filename}`
                                      );
                                    }
                                  );
                      }
                    },
                    function() {
                      console.log(`error when parsing colour for ${record.filename}`);
                      finished++;
                    }
                  );
    },
    function(){
      console.log('Parsing API finished! '+queue.length);
      null;
    }
    );
  });
}

// metadataDao.initDB(DROP_COLLECTION, function() {
//   if (mode == GET_METADATA) {
//     metadataParser.parseSearched(
//       function(records) {
//         console.log(`Parsed ${record.title}`);
//         if (record && UPDATE_DB) {
//           metadataDao.addObject(
//             record,
//             function(createdObject) {},
//             function(err) {
//               console.log(
//                 `Error on ${record.filename}: ${finished} out of ${total}`
//               );
//             }
//           );
//         }
//       },
//       function() {
//         console.log("Finished!");
//       }
//     );
//   }
//   if (mode == DOWNLOAD_IMGS) {
//     metadataDao.findImages(
//       null,
//       1000,
//       function(items) {
//         var imageURLs = items.map(item => item.bigFileURL);
//         imageDownloader.downloadImages(FILE_FOLDER, imageURLs);
//       },
//       function(err) {
//         console.log(err);
//       }
//     );
//   }
//   if (mode == PARSE_COLOURS) {
//     metadataDao.findImages(
//       null,
//       1000, // no limit!
//       function(items) {
//         for (var index in items) {
//           var record = items[index];
//           var filename = imageDownloader.extractFilename(record.fileURL);
//           var localFile = FILE_FOLDER + "/" + filename;
//           colourParser.parse(
//             localFile,
//             record,
//             function(targetMetadata) {
//               // if file not found, the dominant colour will be set to a default value #000000 and palette will not be available
//               // do not put such data into DB

//               var record = targetMetadata;

//               if (UPDATE_DB) {
//                 metadataDao.updateImage(
//                   record,
//                   function(updatedObj) {
//                     console.log(`updated ${updatedObj.title}`);
//                   },
//                   function(err) {
//                     console.log(err);
//                   }
//                 );
//               }
//               // console.log(record);
//               // if (UPDATE_DB) {
//               //   metadataDao.addObject(
//               //     record,
//               //     function(createdObject) {
//               //       finished++;
//               //       console.log(
//               //         `INSERTED ${
//               //           createdObject.filename
//               //         }: ${finished} out of ${total}`
//               //       );
//               //     },
//               //     function(err) {
//               //       console.log(
//               //         `Error on ${record.filename}: ${finished} out of ${total}`
//               //       );
//               //     }
//               //   );
//               // } else {
//               //   // console.log(record);
//               //   finished++;
//               //   console.log(
//               //     `Finished ${record.filename}: ${finished} out of ${total}`
//               //   );
//               // }
//             },
//             function() {
//               console.log(`error when parsing colour for ${record.filename}`);
//               finished++;
//             }
//           );
//         }
//       },
//       function(err) {
//         console.log(err);
//       }
//     );
//   }
// });

